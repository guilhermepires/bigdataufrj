import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object programa {
	def main( args: Array[String]){
		
		val caminhoTxt = "/home/bigdata/dados.txt"
		val conf = new SparkConf().setAppName("programa")
		val sc = new SparkContext(conf)
		val textFile = sc.textFile(caminhoTxt)
		
		//println((() => args(0))())	
		
		if (args(0).toInt ==1){
		//maisFrequentes			
			val numeros = textFile.flatMap(_.split(" "))
			val count = numeros.map((_,1))
			val countsReduce = count.reduceByKey(_+_)
			val ordenado = countsReduce.map( kv => (kv._2, kv._1 ) ).sortByKey(false)
			ordenado.take(50).foreach(println)		
		}else
		if (args(0).toInt ==2){
		//menos frequentes
			val numeros = textFile.flatMap(_.split(" "))
			val count = numeros.map((_,1))
			val countsReduce = count.reduceByKey(_+_)
			val ordenado = countsReduce.map( kv => (kv._2, kv._1 ) ).sortByKey(true)
			ordenado.take(50).foreach(println)
		}else
		if (args(0).toInt ==3){
			//falta implementar para comparar  partes dos numeros, por exemplo comparar só os primeiros 5 numeros de todos os bilhetes
		}
		
	}
}

