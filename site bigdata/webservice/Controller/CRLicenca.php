<?php 
	header('Content-type: application/json; charset=utf-8');

	$conf = json_decode(file_get_contents('configuration.json'), TRUE);
	require_once ($conf['baseDir']."Db.php");
	require_once ($conf['baseDir']."Model/Configuracao.php");

class CRLicenca{
	function inserir($licenca){
		$db = new Db();	
		$message = array();
		if(!$db->error())
			return  $db->query("INSERT INTO Licenca (codigo, usada) VALUES (".$licenca->codigo.",".$licenca->usada.");");
		else
			return -1;						
	}
	
	function getLicencaById($licenca){
		$db = new Db();	
		if(!$db->error())
		{				
			$result= $db->select("SELECT * FROM Licenca WHERE id=".$licenca->id);
			return $result;
		}
	}

	function getLicenca($licenca){
		$db = new Db();	
		if(!$db->error())
		{				
			$result= $db->select("SELECT id,dataInicio,dataFim,contagem,Usuario_id FROM Licenca WHERE codigo=".$licenca->codigo . " AND usada = 0");
			return $result;
		}
		return -1;
	}

	function usarLicenca($licenca)
	{
		$db = new Db();	
		if(!$db->error())
		{				
			$result= $db->query("UPDATE Licenca SET usada = 1 WHERE codigo=".$licenca->codigo);
			return $result;
		}
	}

}
?>
