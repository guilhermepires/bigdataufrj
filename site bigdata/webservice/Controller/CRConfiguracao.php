<?php 
	header('Content-type: application/json; charset=utf-8');

	$conf = json_decode(file_get_contents('configuration.json'), TRUE);
	require_once ($conf['baseDir']."Db.php");
	require_once ($conf['baseDir']."Model/Configuracao.php");

class CRConfiguracao{
	function inserir($objConfiguracao){
		$db = new Db();	
		$message = array();
		if(!$db->error()){	
			
			if(is_null( $objConfiguracao->mantenhaMeLogado))
				$objConfiguracao->mantenhaMeLogado = 1;
			
			if(is_null( $objConfiguracao->urlServidorLocal))
				return	$db->query("INSERT INTO Configuracao (mantenhaMeLogado,Usuario_id) VALUES (".$objConfiguracao->mantenhaMeLogado.",".$objConfiguracao->usuario->id.");");

			return  $db->query("INSERT INTO Configuracao (urlServidorLocal,mantenhaMeLogado,Usuario_id) VALUES (".$objConfiguracao->urlServidorLocal.",".$objConfiguracao->mantenhaMeLogado.",".$objConfiguracao->usuario->id.");");

		}else{
			return -1;	
		}			
	}
function getConfigJson($config){
	$db = new Db();	
	if(!$db->error())
	{			
	
		$result= $db->select("SELECT * FROM Configuracao WHERE id=".$config->id);
		return $result;
	}
}
function setMantenhaLogado($config){
		$db = new Db();	
		$message = array();
		if(!$db->error())
		{			
			$sql = "UPDATE Configuracao SET mantenhaMeLogado=".$config->mantenhaMeLogado." WHERE id=".$config->id." ;";

			$result= $db->update($sql);
			if($result)
				return 1;
			else
				return 3;
			}
		return 2;
}
	function updateConfig($config){
		$db = new Db();	
		$message = array();
		if(!$db->error())
		{			
			$sql = "UPDATE Configuracao SET urlServidorLocal=".$config->urlServidorLocal.",mantenhaMeLogado=".$config->mantenhaMeLogado." WHERE id=".$config->id." ;";			
			$result= $db->update($sql);
			if($result)
				return 1;
			else
				return 3;
		}
		return 2;
	}
}
?>
