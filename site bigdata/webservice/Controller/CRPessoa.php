<?php 
	header('Content-type: application/json; charset=utf-8');
	require_once ('Db.php');

class CRPessoa{
	function inserir($objPessoa){
		$db = new Db();	
		$message = array();
		if(!$db->error())
		{						

			if (is_null($objPessoa->cpf))
				$objPessoa->cpf="null";

			if (is_null($objPessoa->dataDeNascimento))
				$objPessoa->dataDeNascimento="null";

			if (is_null($objPessoa->foto))
				$objPessoa->foto="null";

			return $db->query("INSERT INTO Pessoa (nome,cpf,dataNascimento,foto,Usuario_id) VALUES (".$objPessoa->nome.",".$objPessoa->cpf.",".$objPessoa->dataDeNascimento.",".$objPessoa->foto.",".$objPessoa->Usuario->id.");");

		}else{
			return -1;	
		}			
	}

	function getPessoaPaciente($id)
	{

		$db = new Db();	
		$message = array();
		if(!$db->error())
			return  $db->select("SELECT p.*, p.id as PessoaID, paci.*, paci.id as PacienteID
					FROM Pessoa as p LEFT OUTER JOIN Paciente as paci ON p.id = paci.Pessoa_id WHERE p.id = ". $id);
		else
			return -1;	
	}

	function atualizar($Pessoa, $tipoSanguineo)
	{
		$db = new Db();	
		$message = array();
		if(!$db->error())
		{						
			$db->query("UPDATE Pessoa SET nome = " . $Pessoa->nome . ", cpf = " . $Pessoa->cpf .
				             ", foto = " . $Pessoa->foto . ", dataNascimento = " . $Pessoa->dataDeNascimento . " WHERE id = " . $Pessoa->id);
			
			return $db->query("UPDATE Paciente SET TipoSanguineo_id = " . $tipoSanguineo . " WHERE Pessoa_id = " . $Pessoa->id);
		}else{
			return -1;	
		}			
	}
}
?>
