<?php
	header('Content-type: application/json; charset=utf-8');
	require_once ('Db.php');

	require_once ('./Model/Usuario.php');
	require_once ('./Model/Configuracao.php');

	require_once ('./Controller/CRUsuario.php');
	require_once ('./Controller/CRConfiguracao.php');

	//inicia banco
	$db = new Db();	

	//inicia model
	$usuario = new Usuario();
	$config = new Configuracao();

	//inicia controller
	$crusuario = new CRUsuario();
	$crConfig = new CRConfiguracao();

	//Pega parametros
	$usuario->id = $_POST['Usuario_id'];
	$config->usuario = $usuario;

    $config->id=$_POST['Configuracao_id'];
    $config->urlServidorLocal=$db->quote($_POST['urlServidorLocal']);    
    $config->mantenhaMeLogado=$_POST['mantenhaMeLogado'];

	switch($_POST['idComando']){
		case 1:	
			$result = $crConfig->updateConfig($config);
			switch($result){
				case 1:
					$message["codigo"] = "1";
					$message["resposta"] = "Atualizado";			
				break;
				case 2:
					$message["codigo"] = "2";
					$message["resposta"] = "Erro banco";		
				break;
				case 3:
					$message["codigo"] = "3";
					$message["resposta"] = "Erro update";		
				break;
			}
		break;
		case 2:
			$result = $crConfig->getConfigJson($config);
			$message["codigo"] = "4";
			$message["resposta"] = $result;				
		break;
		case 3:
			$result = $crConfig->setMantenhaLogado($config);
			$message["codigo"] = "3";
			$message["resposta"] = $result;				
		break;
	}
	echo json_encode($message, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHED);
?>
