<?php 

class Db {

	protected static $connection;
	
	public function connect() {
		

		if(!isset(self::$connection))
		{
			$conf = json_decode(file_get_contents('configuration.json'), TRUE);
			self::$connection = new mysqli($conf["host"], $conf["user"], $conf["password"], $conf["database"]);

			self::$connection->set_charset("utf8");
		}
	
		if(self::$connection === false) {
			return false;
		}
		
		return self::$connection;
	}
	public function delete($query) {
		$connection = $this->connect();		
		$result = $connection->query($query);		
		return $result;
	}

	public function update($query){
		$connection = $this->connect();		
		$result = $connection->query($query);
//		return 	mysqli_affected_rows($connection);
return $result;
	}

	public function query($query) {
		$connection = $this->connect();		
		$result = $connection->query($query);

		if($result ==1)
		{
			return 	$connection->insert_id;
		}else
		{
			return -1;
		}
	}

	public function select($query) {
		$connection = $this->connect();		
		$rows = array();
		$result = $connection->query($query);
		if($result === false) {
			return false;
		}
		while ($row = $result->fetch_assoc()) {
			$rows[] = $row;
		}
		return $rows;
	}

	public function selectResult($query) {
		$connection = $this->connect();		
		$rows = array();
		$result = $connection->query($query);
		return $result;
	}

	public function error() {
		$connection = $this -> connect();
		return $connection -> connect_errno;
	}
	
	public function quote($value) {
		$connection = $this -> connect();
		return "'" . $connection -> real_escape_string($value) . "'";
	}
}
