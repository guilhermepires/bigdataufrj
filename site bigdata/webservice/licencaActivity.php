<?php
	header('Content-type: application/json; charset=utf-8');
	require_once ('Db.php');

	require_once ('./Model/Licenca.php');

	require_once ('./Controller/CRLicenca.php');

	//inicia banco
	$db = new Db();	

	//inicia model
	$licenca = new Licenca();

	//inicia controller
	$crlicenca = new CRLicenca();

	//Pega parametros
	switch($_POST['idComando']){
		case 1:	
			$licenca->codigo = $db->quote($_POST['codigo']);
			$result = $crlicenca->getLicenca($licenca);

			if($result == null || is_null($result) || $result == -1)
			{
				$message["codigo"] = "-1";
				$message["resposta"] = "Codigo invalido";				
			}
			else
			{
				$message["codigo"] = "1";
				$message["resposta"] = "Sucesso";
				$message["licenca"] = $result;
				$crlicenca->usarLicenca($licenca);
			}
			break;
	}
	echo json_encode($message, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHED);
?>
