<?php
	header('Content-type: application/json; charset=utf-8');
	require_once ('Db.php');

	require_once ('./Model/Pessoa.php');
	require_once ('./Model/Usuario.php');
	require_once ('./Model/Configuracao.php');

	require_once ('./Controller/CRPessoa.php');
	require_once ('./Controller/CRUsuario.php');
	require_once ('./Controller/CRConfiguracao.php');

	//inicia banco
	$db = new Db();	

	//inicia model
	$usuario = new Usuario();
	$pessoa = new Pessoa();
	$configuracao = new Configuracao();

	//inicia controller
	$crusuario = new CRUsuario();
	$crpessoa = new CRPessoa();	
	$crconfiguracao = new CRConfiguracao();

	//cadastra usuario
	$usuario->email = $db->quote($_POST['email']);
	$usuario->senha = $db->quote($_POST['senha']);

	//verifica se ja possui cadastro no banco 
	$usuarioBanco = $crusuario->getUsuario($usuario);

	if(is_null($usuarioBanco->email)){		
		$usuario->id = $crusuario->inserir($usuario);	
	
		//cadastra pessoa	
		$pessoa->nome=$db->quote($_POST['nome']);
		$pessoa->Usuario = $usuario;
		$pessoa->id= $crpessoa->inserir($pessoa);

		//cadastra configuração	
		$configuracao->usuario = $usuario;
		$configuracao->id = $crconfiguracao->inserir($configuracao);

		if ($usuario->id<0){
			$message["codigo"] = "3";
			$message["resposta"] = "Não foi possivel cadastrar o Usuario";
		}else
		if ($pessoa->id<0){
			$message["codigo"] = "4";
			$message["resposta"] = "Não foi possivel cadastrar o Pessoa";
		}else
		if ($configuracao->id<0){
			$message["codigo"] = "5";
			$message["resposta"] = "Não foi possivel cadastrar a configuracao";
		}else{
			$message["codigo"] = "1";
			$message["resposta"] = "Query efetuada";		
			}
	}
	else{
		$message["codigo"] = "2";
		$message["resposta"] = "Já possui no banco";
	}	
	echo json_encode($message, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHED);
?>
