class Numeros{

    constructor(numero, frequencia){
        this._numero = numero;
        this._frequencia = frequencia;
    }

    getNumero(){
        return this._numero;
    }

    getFrequencia(){
        return this._frequencia;
    }
}
