class HttpService {

    get(url){

        return new Promise((resolve, reject) =>{

            let xhr = new XMLHttpRequest();

            /*Os dados estão vindo do server através da URL abaixo*/
            xhr.open('GET', url);

            /* Para casa mudança de estado executa uma arrow funcition*/
            xhr.onreadystatechange = () => {

                /*
                 0: requisição ainda não iniciada

                 1: conexão com o servidor estabelecida

                 2: requisição recebida

                 3: processando requisição

                 4: requisição está concluída e a resposta está pronta
                 */
                if(xhr.readyState === 4){

                    if(xhr.status === 200){
                        console.log("Requisição feita com sucesso!");

                        /*
                         Dados obtidos do servidor
                         xhr.responseText  '[["47", 404], ["41", 396]]...'
                         */
						let dados = JSON.parse("["+xhr.responseText+"]");
						sessionStorage.setItem("jogos",dados);

                        resolve(xhr.responseText);

                    }else{
                        console.log(xhr.responseText);
                      //  reject("Não foi possível realizar a requisição");
                    }
                }
            };
            xhr.send();
        })
    }
}
