class NumerosView{
    constructor(elemento){
        this._elemento = elemento;
    }


    _template(dados){

      // Remove o primeiro e o ultimo colchete.. [[12, 19],[39,10]] para [12, 19],[39,10]
      return dados;

    }


    atualizar(dados){
        /**
         * O innerHTML irá converter a string do template para o dom do html.
         */
        this._elemento.innerHTML = this._template(dados);
    }
}
