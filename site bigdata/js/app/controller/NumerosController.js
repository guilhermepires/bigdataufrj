class NumerosController{

    constructor(){

        /**
         * Pega a DIV da page e cria uma instancia na sua classe template,
         * passado para a classe NumerosView o elemento da div que será incluída o template.
         */
        this._numerosView = new NumerosView(document.querySelector('#divNumeroView'));

    }

    /**
     * Chama o serviço que disponibiliza os dados da Lotomania.
     */
    importarJogos(){

         Promise.resolve(new NumerosService().obterContagem()).then(dados =>{
                this._numerosView.atualizar(dados);
        });
    }
}

